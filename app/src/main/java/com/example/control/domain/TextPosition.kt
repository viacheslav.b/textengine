package com.example.control.domain


interface TextPosition

data class ExactTextPosition(
    val x: Int,
    val y: Int,
) : TextPosition

data class RelationTextPosition(
    val xRel: Int,
    val yRel: Int,
) : TextPosition
