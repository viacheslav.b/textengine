package com.example.control.domain

data class TextPadding(
    val _rim: Int? = null,
    val _left: Int? = null,
    val _right: Int? = null,
    val _top: Int? = null,
    val _bottom: Int? = null,
) {
    val rim: Int get() = _rim ?: 0
    val left: Int get() = _left ?: 0
    val right: Int get() = _right ?: 0
    val top: Int get() = _top ?: 0
    val bottom: Int get() = _bottom ?: 0

    val leftCalculated: Int get() = getField(_left)
    val rightCalculated: Int get() = getField(_right)
    val topCalculated: Int get() = getField(_top)
    val bottomCalculated: Int get() = getField(_bottom)

    private fun getField(field: Int?): Int = field?.takeIf { it > 0 } ?: rim

}