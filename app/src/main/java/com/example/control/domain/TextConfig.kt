package com.example.control.domain

import android.util.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import java.util.UUID

enum class TextConfigAlign {
    Left, Center, Right,
}

fun TextConfigAlign.toTextAlign() = when (this) {
    TextConfigAlign.Left -> TextAlign.Left
    TextConfigAlign.Center -> TextAlign.Center
    TextConfigAlign.Right -> TextAlign.Right

}

enum class TextConfigFont {
    Regular, Medium,
}

data class RGBColor(
    val red: Int = 255,
    val green: Int = 255,
    val blue: Int = 255,
    val alpha: Int = 255,
) {
    fun toColor(): Color = Color(
        red = red,
        green = green,
        blue = blue,
        alpha = alpha,
    )
}


data class TextConfig(
    //done
    val input: Boolean = false,                               //6.1  # 32
    val content: String = "",                                 //6.2  # 20
    val defaultText: String? = null,                           //6.7  # 21
    val font: TextConfigFont = TextConfigFont.Regular,        //6.3  # 29
    val fontSize: Int = 12,                                   //6.4  # 30
    val color: RGBColor = RGBColor(red = 0, green = 0, blue = 0),   //6.5  # 15 – 19
    val background: RGBColor = RGBColor(alpha = 0),              //6.9  # 2 - 7
    val borderWidth: Int = 0,                                       //6.10 # 13
    val borderColor: RGBColor = RGBColor(red = 0, green = 0, blue = 0),             //6.11 # 8 - 12
    val radius: Int = 0,                                            //6.12 # 45
    val position: ExactTextPosition = ExactTextPosition(0, 0), //6.13 # 40 – 44
    val padding: TextPadding = TextPadding(),                      //6.17 # 47, 35, 46, 14
    val align: TextConfigAlign = TextConfigAlign.Left,             //6.18
    val lines: Int = 1,                                            //6.15 # 36
    val size: Size = Size(50, 50),                      //6.23 # 64 – 68
    val identifier: String = UUID.randomUUID().toString(),        //6.31


    val scroll: Boolean = false,                                   //6.16 # 48
    val underlineThickness: Int = 0,                               //6.20 # 76
    val underlineColor: RGBColor = RGBColor(),                     //6.21 # 70 – 75
    val textUrl: TextUrl = TextUrl(),                              //6.22 # 77 – 95


    val firstResponder: Boolean = false,                     //6.26 # 28
    val nextResponderIdentifier: String = "",                 //6.27 # 39

    //input field
    val maxStrokes: Int? = null,                              //6.28 # 38
    val executionDelayMillis: Int = 0,
    val isSecureTextEntry: Boolean = false,


    val lineSpace: Int = 0,                                        //6.14 # 37
//    val shadow: Size = Size(0, 0),                              //6.24 # 50 - 63
//    val KeyboardType
//     val InputFieldHeightDynamic


//    val defaultTextColor: RGBColor = RGBColor(),       //6.8  # 22 - 26 not needed, same that color


)