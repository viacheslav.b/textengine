package com.example.control.ui.screen

import androidx.activity.ComponentActivity
import com.example.control.R

import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.*
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.control.ui.viewmodel.MainViewModel
import com.example.control.ui.widget.NumberFieldWidget

@Composable
fun SetupNumberScreen(
    onNavigateNext: () -> Unit,
    viewModel: MainViewModel = viewModel(LocalContext.current as ComponentActivity),
) {


    Column(
        modifier = Modifier.fillMaxSize(),
//        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {

        Text(text = stringResource(R.string.setup_number_title), fontSize = 30.sp)

        Spacer(modifier = Modifier.height(250.dp))


        ChangeNumberContent(viewModel)

        Spacer(modifier = Modifier.height(250.dp))

        TextButton(onClick = onNavigateNext) {
            Icon(
                Icons.Rounded.ArrowForward,
                modifier = Modifier.size(80.dp),
                contentDescription = "",
            )
        }
    }


}

@Composable
private fun ChangeNumberContent(viewModel: MainViewModel) {

    val state = viewModel.uiState.collectAsState()

    NumberFieldWidget(
        text = state.value.textFieldNumber.toString(),
        onDecrease = viewModel::decreaseNumberOfTextFields,
        onIncrease = viewModel::increaseNumberOfTextFields,
        sizeOfContent = 60,
    )
}