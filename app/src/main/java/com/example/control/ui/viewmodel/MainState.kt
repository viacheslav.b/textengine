package com.example.control.ui.viewmodel

import com.example.control.domain.TextConfig
import kotlin.math.min

data class MainState(
    val textFieldNumber: Int = 1,
    val editingActiveSettingIndexField: Int = 0,
    val configListField: List<TextConfig> = List(TEXT_FIELDS_NUMBER_MAX) { TextConfig() },
) {

    val activeConfigList: List<TextConfig> get() = configListField.take(textFieldNumber)
    val activeConfig: TextConfig get() = activeConfigList[setupEditingActiveSettingIndex]

    val setupEditingActiveSettingIndex
        get() = min(
            editingActiveSettingIndexField,
            textFieldNumber - 1
        )


    companion object {
        const val TEXT_FIELDS_NUMBER_MIN = 1
        const val TEXT_FIELDS_NUMBER_MAX = 7

        const val TEXT_SIZE_MIN = 1
        const val TEXT_SIZE_MAX = 50

        const val BORDER_WIDTH_MIN = 0
        const val BORDER_WIDTH_MAX = 20

        const val BORDER_RADIUS_MIN = 0
        const val BORDER_RADIUS_MAX = 20

        const val SIZE_WIDTH_MIN = 0
        const val SIZE_WIDTH_MAX = 2000

        const val LINES_MIN = 1
        const val LINES_MAX = 100
    }
}