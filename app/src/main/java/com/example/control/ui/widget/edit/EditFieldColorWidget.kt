package com.example.control.ui.widget.edit

import androidx.compose.foundation.layout.Column
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.res.stringResource
import com.example.control.R
import com.example.control.domain.TextConfig
import com.example.control.ui.viewmodel.MainViewModel


@Composable
fun EditFieldColorWidget(
    activeConfig: TextConfig,
    viewModel: MainViewModel,
) {
    Column(horizontalAlignment = Alignment.CenterHorizontally) {

        TextField(
            value = activeConfig.color.red.toString(),
            onValueChange = { viewModel.editColor(red = it) },
            label = { Text(text = stringResource(R.string.setup_text_fields_color_red)) },
        )

        TextField(
            value = activeConfig.color.green.toString(),
            onValueChange = { viewModel.editColor(green = it) },
            label = { Text(text = stringResource(R.string.setup_text_fields_color_green)) },
        )

        TextField(
            value = activeConfig.color.blue.toString(),
            onValueChange = { viewModel.editColor(blue = it) },
            label = { Text(text = stringResource(R.string.setup_text_fields_color_blue)) },
        )

        TextField(
            value = activeConfig.color.alpha.toString(),
            onValueChange = { viewModel.editColor(alpha = it) },
            label = { Text(text = stringResource(R.string.setup_text_fields_color_alpha)) },
        )
    }

}


@Composable
fun EditFieldBackgroundColorWidget(
    activeConfig: TextConfig,
    viewModel: MainViewModel,
) {
    Column(horizontalAlignment = Alignment.CenterHorizontally) {

        TextField(
            value = activeConfig.background.red.toString(),
            onValueChange = { viewModel.editBackgroundColor(red = it) },
            label = { Text(text = stringResource(R.string.setup_text_fields_color_red)) },
        )

        TextField(
            value = activeConfig.background.green.toString(),
            onValueChange = { viewModel.editBackgroundColor(green = it) },
            label = { Text(text = stringResource(R.string.setup_text_fields_color_green)) },
        )

        TextField(
            value = activeConfig.background.blue.toString(),
            onValueChange = { viewModel.editBackgroundColor(blue = it) },
            label = { Text(text = stringResource(R.string.setup_text_fields_color_blue)) },
        )

        TextField(
            value = activeConfig.background.alpha.toString(),
            onValueChange = { viewModel.editBackgroundColor(alpha = it) },
            label = { Text(text = stringResource(R.string.setup_text_fields_color_alpha)) },
        )
    }

}


@Composable
fun EditFieldBorderColorWidget(
    activeConfig: TextConfig,
    viewModel: MainViewModel,
) {
    Column(horizontalAlignment = Alignment.CenterHorizontally) {

        TextField(
            value = activeConfig.borderColor.red.toString(),
            onValueChange = { viewModel.editBorderColor(red = it) },
            label = { Text(text = stringResource(R.string.setup_text_fields_color_red)) },
        )

        TextField(
            value = activeConfig.borderColor.green.toString(),
            onValueChange = { viewModel.editBorderColor(green = it) },
            label = { Text(text = stringResource(R.string.setup_text_fields_color_green)) },
        )

        TextField(
            value = activeConfig.borderColor.blue.toString(),
            onValueChange = { viewModel.editBorderColor(blue = it) },
            label = { Text(text = stringResource(R.string.setup_text_fields_color_blue)) },
        )

        TextField(
            value = activeConfig.borderColor.alpha.toString(),
            onValueChange = { viewModel.editBorderColor(alpha = it) },
            label = { Text(text = stringResource(R.string.setup_text_fields_color_alpha)) },
        )
    }

}