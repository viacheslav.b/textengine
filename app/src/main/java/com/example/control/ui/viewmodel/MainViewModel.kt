package com.example.control.ui.viewmodel

import android.util.Size
import androidx.lifecycle.ViewModel
import com.example.control.domain.ExactTextPosition
import com.example.control.domain.TextConfig
import com.example.control.domain.TextConfigAlign
import com.example.control.domain.TextConfigFont
import com.example.control.ui.viewmodel.MainState.Companion.BORDER_RADIUS_MAX
import com.example.control.ui.viewmodel.MainState.Companion.BORDER_RADIUS_MIN
import com.example.control.ui.viewmodel.MainState.Companion.BORDER_WIDTH_MAX
import com.example.control.ui.viewmodel.MainState.Companion.BORDER_WIDTH_MIN
import com.example.control.ui.viewmodel.MainState.Companion.LINES_MAX
import com.example.control.ui.viewmodel.MainState.Companion.LINES_MIN
import com.example.control.ui.viewmodel.MainState.Companion.SIZE_WIDTH_MAX
import com.example.control.ui.viewmodel.MainState.Companion.SIZE_WIDTH_MIN
import com.example.control.ui.viewmodel.MainState.Companion.TEXT_FIELDS_NUMBER_MAX
import com.example.control.ui.viewmodel.MainState.Companion.TEXT_FIELDS_NUMBER_MIN
import com.example.control.ui.viewmodel.MainState.Companion.TEXT_SIZE_MAX
import com.example.control.ui.viewmodel.MainState.Companion.TEXT_SIZE_MIN
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import java.lang.Integer.min
import kotlin.math.max

class MainViewModel : ViewModel() {

    private val _uiState = MutableStateFlow(MainState())
    val uiState: StateFlow<MainState> = _uiState.asStateFlow()

    //set up number
    fun increaseNumberOfTextFields() {
        val state = _uiState.value
        val newState =
            state.copy(textFieldNumber = min(state.textFieldNumber + 1, TEXT_FIELDS_NUMBER_MAX))
        _uiState.value = newState
    }

    fun decreaseNumberOfTextFields() {
        val state = _uiState.value
        val newState =
            state.copy(textFieldNumber = max(state.textFieldNumber - 1, TEXT_FIELDS_NUMBER_MIN))
        _uiState.value = newState
    }


    //set up text fields
    fun setupTextFieldOnIndexChanged(index: Int) {
        val state = _uiState.value
        val newState = state.copy(editingActiveSettingIndexField = index)
        _uiState.value = newState
    }

    fun editingOnCheckedChanged() {
        updateFields { activeConfig ->

            val isInputActivated = !activeConfig.input

            return@updateFields activeConfig.copy(
                input = isInputActivated,
                content = if (!isInputActivated) "" else activeConfig.content,
                defaultText = if (isInputActivated) activeConfig.defaultText else ""
            )
        }
    }

    fun editingOnContentChanged(content: String) {
        updateFields { activeConfig ->
            return@updateFields activeConfig.copy(content = content)
        }
    }

    fun editingOnDefaultTextChanged(defaultText: String) {
        updateFields { activeConfig ->
            return@updateFields activeConfig.copy(defaultText = defaultText)
        }
    }

    fun editingFontSizeIncrease() {
        updateFields { activeConfig ->
            val size = min(activeConfig.fontSize + 1, TEXT_SIZE_MAX)
            return@updateFields activeConfig.copy(fontSize = size)
        }
    }

    fun editingFontSizeDecrease() {
        updateFields { activeConfig ->
            val size = max(activeConfig.fontSize - 1, TEXT_SIZE_MIN)
            return@updateFields activeConfig.copy(fontSize = size)
        }
    }

    fun editingBorderWidthIncrease() {
        updateFields { activeConfig ->
            val width = min(activeConfig.borderWidth + 1, BORDER_WIDTH_MAX)
            return@updateFields activeConfig.copy(borderWidth = width)
        }
    }

    fun editingBorderWidthDecrease() {
        updateFields { activeConfig ->
            val width = max(activeConfig.borderWidth - 1, BORDER_WIDTH_MIN)
            return@updateFields activeConfig.copy(borderWidth = width)
        }
    }

    fun editingBorderRadiusIncrease() {
        updateFields { activeConfig ->
            val radius = min(activeConfig.radius + 1, BORDER_RADIUS_MAX)
            return@updateFields activeConfig.copy(radius = radius)
        }
    }

    fun editingBorderRadiusDecrease() {
        updateFields { activeConfig ->
            val radius = max(activeConfig.radius - 1, BORDER_RADIUS_MIN)
            return@updateFields activeConfig.copy(radius = radius)
        }
    }

    fun editingMaxLinesIncrease() {
        updateFields { activeConfig ->
            val lines = activeConfig.lines
            val newLines = min(lines + 1, LINES_MAX)
            return@updateFields activeConfig.copy(lines = newLines)
        }
    }

    fun editingMaxLinesDecrease() {
        updateFields { activeConfig ->
            val lines = activeConfig.lines
            val newLines = max(lines - 1, LINES_MIN)
            return@updateFields activeConfig.copy(lines = newLines)
        }
    }

    fun editingSizeHeightIncrease() {
        updateFields { activeConfig ->
            val size = activeConfig.size
            val height = min(size.height + 10, SIZE_WIDTH_MAX)
            return@updateFields activeConfig.copy(size = Size(size.width, height))
        }
    }

    fun editingSizeHeightDecrease() {
        updateFields { activeConfig ->
            val size = activeConfig.size
            val height = max(size.height - 10, SIZE_WIDTH_MIN)
            return@updateFields activeConfig.copy(size = Size(size.width, height))
        }
    }

    fun editingSizeWidthIncrease() {
        updateFields { activeConfig ->
            val size = activeConfig.size
            val width = min(size.width + 10, SIZE_WIDTH_MAX)
            return@updateFields activeConfig.copy(size = Size(width, size.height))
        }
    }

    fun editingSizeWidthDecrease() {
        updateFields { activeConfig ->
            val size = activeConfig.size
            val width = max(size.width - 10, SIZE_WIDTH_MIN)
            return@updateFields activeConfig.copy(size = Size(width, size.height))
        }
    }

    fun editingSetFont(font: TextConfigFont) {
        updateFields { activeConfig ->
            return@updateFields activeConfig.copy(font = font)
        }
    }

    fun editingSetAlign(align: TextConfigAlign) {
        updateFields { activeConfig ->
            return@updateFields activeConfig.copy(align = align)
        }
    }


    fun editColor(
        red: String? = null,
        green: String? = null,
        blue: String? = null,
        alpha: String? = null,
    ) {

        updateFields { activeConfig ->
            val color = activeConfig.color
            val newColor = color.copy(
                red = red.toColorInt(color.red),
                green = green.toColorInt(color.green),
                blue = blue.toColorInt(color.blue),
                alpha = alpha.toColorInt(color.alpha),
            )
            return@updateFields activeConfig.copy(color = newColor)
        }
    }

    fun editBackgroundColor(
        red: String? = null,
        green: String? = null,
        blue: String? = null,
        alpha: String? = null,
    ) {

        updateFields { activeConfig ->
            val color = activeConfig.background
            val newColor = color.copy(
                red = red.toColorInt(color.red),
                green = green.toColorInt(color.green),
                blue = blue.toColorInt(color.blue),
                alpha = alpha.toColorInt(color.alpha),
            )
            return@updateFields activeConfig.copy(background = newColor)
        }
    }

    fun editBorderColor(
        red: String? = null,
        green: String? = null,
        blue: String? = null,
        alpha: String? = null,
    ) {

        updateFields { activeConfig ->
            val borderColor = activeConfig.borderColor
            val newBorderColor = borderColor.copy(
                red = red.toColorInt(borderColor.red),
                green = green.toColorInt(borderColor.green),
                blue = blue.toColorInt(borderColor.blue),
                alpha = alpha.toColorInt(borderColor.alpha),
            )
            return@updateFields activeConfig.copy(borderColor = newBorderColor)
        }
    }

    fun editPosition(
        x: String? = null,
        y: String? = null,
    ) {
        updateFields { activeConfig ->
            val position = activeConfig.position
            val newPosition = ExactTextPosition(
                x = x.toPositionInt(position.x),
                y = y.toPositionInt(position.y),

                )
            return@updateFields activeConfig.copy(position = newPosition)
        }
    }

    fun editPadding(
        rim: String? = null,
        left: String? = null,
        right: String? = null,
        top: String? = null,
        bottom: String? = null,
    ) {
        updateFields { activeConfig ->
            val padding = activeConfig.padding
            val newPadding = padding.copy(
                _rim = rim.toPaddingInt(padding._rim),
                _left = left.toPaddingInt(padding._left),
                _right = right.toPaddingInt(padding._right),
                _top = top.toPaddingInt(padding._top),
                _bottom = bottom.toPaddingInt(padding._bottom),
            )
            return@updateFields activeConfig.copy(padding = newPadding)
        }
    }

    private fun updateFields(updateActiveConfig: (TextConfig) -> TextConfig) {
        val configsField = _uiState.value.configListField.toMutableList()
        val activeConfig = _uiState.value.activeConfig
        val updatedConfig = updateActiveConfig(activeConfig)

        val index = configsField.indexOf(activeConfig)
        configsField[index] = updatedConfig

        val newState = _uiState.value.copy(configListField = configsField)
        _uiState.value = newState
    }

    private fun String?.toColorInt(default: Int): Int {
        var value = this?.toIntOrNull() ?: return default
        value = max(0, value)
        value = min(255, value)
        return value
    }

    private fun String?.toPositionInt(default: Int): Int {
        var value = this?.toIntOrNull() ?: return default
        value = max(0, value)
        value = min(2000, value)
        return value
    }

    private fun String?.toPaddingInt(default: Int?): Int? {
        if (this?.isEmpty() == true) return 0

        var value = this?.toIntOrNull() ?: return default
        value = max(0, value)
        value = min(50, value)
        return value
    }
}