package com.example.control.ui.widget

import android.util.Size
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.control.R
import com.example.control.domain.*


@ExperimentalMaterial3Api
@Composable
fun TextConfigWidget(config: TextConfig) {
    Box(modifier = Modifier.getSizeModifier(config.size)) {
        TextConfigWidgetText(config)
    }

}

fun Modifier.getSizeModifier(size: Size?): Modifier{
    if (size == null){
        return  this
    }

    return this.width(size.width.dp).let {
        return@let if (size.height != 0) {
            it.height(size.height.dp)
        } else {
            it
        }
    }
}

@ExperimentalMaterial3Api
@Composable
fun TextConfigWidgetText(config: TextConfig) {


    if (config.input) {
        TextConfigInputField(
            defaultText = config.defaultText ?: "",
            color = config.color.toColor(),
            backgroundColor = config.background.toColor(),
            position = config.position,

            textSize = config.fontSize,
            font = config.font,
            textAlign = config.align,

            borderWidth = config.borderWidth,
            borderColor = config.borderColor.toColor(),
            borderRadius = config.radius,

            padding = config.padding,

            lines = config.lines,
        )
    } else {
        TextConfigText(
            content = config.content,
            color = config.color.toColor(),
            backgroundColor = config.background.toColor(),
            position = config.position,

            textSize = config.fontSize,
            font = config.font,
            textAlign = config.align,

            borderWidth = config.borderWidth,
            borderColor = config.borderColor.toColor(),
            borderRadius = config.radius,

            padding = config.padding,

            lines = config.lines,
        )
    }

}

@ExperimentalMaterial3Api
@Composable
fun TextConfigInputField(
    defaultText: String,
    color: Color,
    backgroundColor: Color,
    position: ExactTextPosition,

    textSize: Int,
    font: TextConfigFont,
    textAlign: TextConfigAlign,

    borderWidth: Int,
    borderColor: Color,
    borderRadius: Int,

    padding: TextPadding,

    lines: Int,

) {
    var textValue by remember { mutableStateOf(TextFieldValue(defaultText)) }

    TextField(
        value = textValue,
        onValueChange = {
            textValue = it
        },
        maxLines = lines,
        textStyle = getTextStyle(
            textSize = textSize,
            font = font, textAlign = textAlign
        ).copy(
            color = color
        ),
        modifier = Modifier
            .background(backgroundColor)
            .offset(x = position.x.dp, y = position.y.dp)
            .border(
                width = borderWidth.dp,
                color = borderColor,
                shape = RoundedCornerShape(borderRadius.dp),
            )
            .padding(
                start = padding.leftCalculated.dp,
                end = padding.rightCalculated.dp,
                top = padding.topCalculated.dp,
                bottom = padding.bottomCalculated.dp,
            )

    )
}


@Composable
fun TextConfigText(
    content: String,
    color: Color,
    backgroundColor: Color,
    position: ExactTextPosition,

    textSize: Int,
    font: TextConfigFont,
    textAlign: TextConfigAlign,

    borderWidth: Int,
    borderColor: Color,
    borderRadius: Int,

    padding: TextPadding,

    lines: Int,
) {
    Text(
        text = content,
        color = color,
        maxLines = lines,

        style = getTextStyle(textSize = textSize, font = font, textAlign = textAlign),
        modifier = Modifier
            .background(color = backgroundColor, shape = RectangleShape)
            .offset(x = position.x.dp, y = position.y.dp)
            .border(
                width = borderWidth.dp,
                color = borderColor,
                shape = RoundedCornerShape(borderRadius.dp),
            )
            .padding(
                start = padding.leftCalculated.dp,
                end = padding.rightCalculated.dp,
                top = padding.topCalculated.dp,
                bottom = padding.bottomCalculated.dp,
            )
    )
}


fun getTextStyle(
    textSize: Int,
    font: TextConfigFont,
    textAlign: TextConfigAlign,
): TextStyle {

    val fontId = when (font) {
        TextConfigFont.Regular -> R.font.regular
        TextConfigFont.Medium -> R.font.medium
    }
    val fontFamily = FontFamily(Font(fontId))

    val lineHeight = 16.0.sp
    val weight = FontWeight.Medium
    val tracking = 0.5.sp

    return TextStyle(
        fontFamily = fontFamily,
        lineHeight = lineHeight,
        fontWeight = weight,
        fontSize = textSize.sp,
        letterSpacing = tracking,
        textAlign = textAlign.toTextAlign(),
    )
}