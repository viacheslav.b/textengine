package com.example.control.ui.widget.edit

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.sp

@Composable
fun Title(
    stringId: Int
) {

    Text(
        text = stringResource(stringId),
        fontSize = 20.sp
    )
}
