package com.example.control.ui.widget

import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material.icons.rounded.Remove
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun NumberFieldWidget(
    text: String,
    onDecrease: () -> Unit,
    onIncrease: () -> Unit,
    sizeOfContent: Int = 30,
) {
    Row(
        modifier = Modifier.wrapContentHeight(),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically,
    ) {

        TextButton(onClick = onDecrease) {
            Icon(
                Icons.Rounded.Remove,
                modifier = Modifier.size(sizeOfContent.dp),
                contentDescription = "",
            )
        }

        Spacer(modifier = Modifier.width(sizeOfContent.dp))

        Text(text = text, fontSize = sizeOfContent.sp)

        Spacer(modifier = Modifier.width(sizeOfContent.dp))

        TextButton(onClick = onIncrease) {
            Icon(
                Icons.Rounded.Add,
                modifier = Modifier.size(sizeOfContent.dp),
                contentDescription = "",
            )
        }
    }
}