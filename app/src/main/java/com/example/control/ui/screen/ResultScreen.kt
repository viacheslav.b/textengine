package com.example.control.ui.screen

import androidx.activity.ComponentActivity
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.control.R
import com.example.control.ui.viewmodel.MainViewModel
import com.example.control.ui.widget.TextConfigWidget

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ResultScreen(
    onNavigateBack: () -> Unit,
    viewModel: MainViewModel = viewModel(LocalContext.current as ComponentActivity),
) {
    val state = viewModel.uiState.collectAsState().value

    Column(modifier = Modifier.fillMaxSize()) {
        TabBar(onNavigateBack = onNavigateBack)

        Box(modifier = Modifier.fillMaxSize()) {
            for (config in state.activeConfigList) {
                TextConfigWidget(config)
            }

        }
    }
}

@Composable
private fun TabBar(
    onNavigateBack: () -> Unit,
) {
    Row(verticalAlignment = Alignment.CenterVertically) {
        TextButton(onClick = onNavigateBack) {
            Icon(
                Icons.Rounded.ArrowBack,
                modifier = Modifier.size(45.dp),
                contentDescription = "",
            )
        }

        Text(
            text = stringResource(R.string.result_title),
            fontSize = 30.sp,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .weight(1f)
                .padding(end = 70.dp)
        )

    }
}