package com.example.control.ui.screen

import androidx.activity.ComponentActivity
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Checkbox
import androidx.compose.material.RadioButton
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ArrowBack
import androidx.compose.material.icons.rounded.ArrowForward
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.control.R
import com.example.control.domain.TextConfig
import com.example.control.domain.TextConfigAlign
import com.example.control.domain.TextConfigFont
import com.example.control.ui.viewmodel.MainState
import com.example.control.ui.viewmodel.MainViewModel
import com.example.control.ui.widget.NumberFieldWidget
import com.example.control.ui.widget.edit.EditFieldBackgroundColorWidget
import com.example.control.ui.widget.edit.EditFieldBorderColorWidget
import com.example.control.ui.widget.edit.EditFieldColorWidget
import com.example.control.ui.widget.edit.Title

@Composable
fun SetupTextFieldsScreen(
    onNavigateBack: () -> Unit,
    onNavigateNext: () -> Unit,
    viewModel: MainViewModel = viewModel(LocalContext.current as ComponentActivity),
) {
    val state = viewModel.uiState.collectAsState().value

    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {

        TabBar(
            state = state,
            onNavigateBack = onNavigateBack,
            onNavigateNext = onNavigateNext,
            viewModel = viewModel,
        )


        Spacer(modifier = Modifier.height(10.dp))

        EditingContent(state = state, viewModel = viewModel)

    }


}

@Composable
private fun TabBar(
    state: MainState,
    onNavigateBack: () -> Unit,
    onNavigateNext: () -> Unit,
    viewModel: MainViewModel,
) {

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {

        Row(verticalAlignment = Alignment.CenterVertically) {
            TextButton(onClick = onNavigateBack) {
                Icon(
                    Icons.Rounded.ArrowBack,
                    modifier = Modifier.size(45.dp),
                    contentDescription = "",
                )
            }

            Text(
                text = stringResource(R.string.setup_text_fields_title),
                fontSize = 30.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier.weight(1f)
            )


            TextButton(onClick = onNavigateNext) {
                Icon(
                    Icons.Rounded.ArrowForward,
                    modifier = Modifier.size(45.dp),
                    contentDescription = "",
                )
            }
        }

        LazyRow(
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            items(count = state.textFieldNumber) { index ->
                Tab(
                    index = index,
                    state = state,
                    onIndexChanged = viewModel::setupTextFieldOnIndexChanged,
                    modifier = Modifier
                        .weight(1f)
                        .padding(horizontal = 8.dp),
                )
            }
        }
    }
}


@Composable
private fun Tab(
    index: Int,
    state: MainState,
    onIndexChanged: (Int) -> Unit,
    modifier: Modifier,
) {
    val isSelected = state.setupEditingActiveSettingIndex == index
    val color =
        if (isSelected) MaterialTheme.colorScheme.primary
        else Color.Unspecified

    val fontSize =
        if (isSelected) 55.sp
        else 40.sp

    TextButton(onClick = { onIndexChanged(index) }) {
        Text(
            text = (index + 1).toString(),
            fontSize = fontSize,
            color = color,
            modifier = modifier,
        )
    }
}

@Composable
private fun EditingContent(
    state: MainState,
    viewModel: MainViewModel,
) {
    val activeConfig = state.activeConfig

    Title(R.string.setup_text_fields_input)
    Checkbox(
        checked = activeConfig.input,
        onCheckedChange = { viewModel.editingOnCheckedChanged() })
    Spacer(modifier = Modifier.height(15.dp))

    //only no input
    Content(activeConfig = activeConfig, viewModel = viewModel)
    //only no input

    //only input
    DefaultText(activeConfig = activeConfig, viewModel = viewModel)
    //only input


    Font(activeConfig = activeConfig, viewModel = viewModel)
    Spacer(modifier = Modifier.height(15.dp))

    FontSize(activeConfig = activeConfig, viewModel = viewModel)
    Spacer(modifier = Modifier.height(15.dp))
    
    Lines(activeConfig = activeConfig, viewModel = viewModel)
    Spacer(modifier = Modifier.height(15.dp))

    SizeHeight(activeConfig = activeConfig, viewModel = viewModel)
    Spacer(modifier = Modifier.height(15.dp))

    SizeWidth(activeConfig = activeConfig, viewModel = viewModel)
    Spacer(modifier = Modifier.height(15.dp))

    Title(R.string.setup_text_fields_color)
    EditFieldColorWidget(activeConfig = activeConfig, viewModel = viewModel)
    Spacer(modifier = Modifier.height(15.dp))

    Title(R.string.setup_text_fields_background_color)
    EditFieldBackgroundColorWidget(activeConfig = activeConfig, viewModel = viewModel)
    Spacer(modifier = Modifier.height(15.dp))

    BorderWidth(activeConfig = activeConfig, viewModel = viewModel)
    Spacer(modifier = Modifier.height(15.dp))

    Title(R.string.setup_text_fields_border_color)
    EditFieldBorderColorWidget(activeConfig = activeConfig, viewModel = viewModel)
    Spacer(modifier = Modifier.height(15.dp))

    BorderRadius(activeConfig = activeConfig, viewModel = viewModel)
    Spacer(modifier = Modifier.height(15.dp))

    Position(activeConfig = activeConfig, viewModel = viewModel)
    Spacer(modifier = Modifier.height(15.dp))

    Padding(activeConfig = activeConfig, viewModel = viewModel)
    Spacer(modifier = Modifier.height(15.dp))

    Align(activeConfig = activeConfig, viewModel = viewModel)
    Spacer(modifier = Modifier.height(15.dp))


}


@Composable
private fun Content(
    activeConfig: TextConfig,
    viewModel: MainViewModel,
) {
    if (activeConfig.input) {
        return
    }

    Title(R.string.setup_text_fields_content)
    TextField(
        value = activeConfig.content,
        onValueChange = { value -> viewModel.editingOnContentChanged(value) },
    )
    Spacer(modifier = Modifier.height(15.dp))
}

@Composable
private fun DefaultText(
    activeConfig: TextConfig,
    viewModel: MainViewModel,
) {
    if (!activeConfig.input) {
        return
    }

    Title(R.string.setup_text_fields_default_text)
    TextField(
        value = activeConfig.defaultText ?: "",
        onValueChange = { value -> viewModel.editingOnDefaultTextChanged(value) },
    )
    Spacer(modifier = Modifier.height(15.dp))
}

@Composable
private fun Font(
    activeConfig: TextConfig,
    viewModel: MainViewModel,
) {
    Title(R.string.setup_text_fields_font)
    Row(verticalAlignment = Alignment.CenterVertically) {
        Text(
            text = stringResource(R.string.setup_text_fields_font_regular),
            fontSize = 15.sp,
            modifier = Modifier.padding(horizontal = 20.dp),
        )
        RadioButton(
            selected = activeConfig.font == TextConfigFont.Regular,
            onClick = { viewModel.editingSetFont(TextConfigFont.Regular) },
            enabled = true,
        )

        Spacer(modifier = Modifier.weight(1f))

        Text(
            text = stringResource(R.string.setup_text_fields_font_medium),
            fontSize = 15.sp
        )
        RadioButton(
            selected = activeConfig.font == TextConfigFont.Medium,
            onClick = { viewModel.editingSetFont(TextConfigFont.Medium) },
            enabled = true,
            modifier = Modifier.padding(horizontal = 20.dp),
        )

    }

}


@Composable
private fun FontSize(
    activeConfig: TextConfig,
    viewModel: MainViewModel,
) {
    Title(R.string.setup_text_fields_font_size)
    NumberFieldWidget(
        text = activeConfig.fontSize.toString(),
        onIncrease = viewModel::editingFontSizeIncrease,
        onDecrease = viewModel::editingFontSizeDecrease,
    )
}

@Composable
private fun BorderWidth(
    activeConfig: TextConfig,
    viewModel: MainViewModel,
) {
    Title(R.string.setup_text_fields_border_width)
    NumberFieldWidget(
        text = activeConfig.borderWidth.toString(),
        onIncrease = viewModel::editingBorderWidthIncrease,
        onDecrease = viewModel::editingBorderWidthDecrease,
    )
}


@Composable
private fun BorderRadius(
    activeConfig: TextConfig,
    viewModel: MainViewModel,
) {
    Title(R.string.setup_text_fields_border_radius)
    NumberFieldWidget(
        text = activeConfig.radius.toString(),
        onIncrease = viewModel::editingBorderRadiusIncrease,
        onDecrease = viewModel::editingBorderRadiusDecrease,
    )
}

@Composable
private fun Position(
    activeConfig: TextConfig,
    viewModel: MainViewModel,
) {
    Title(R.string.setup_text_fields_position)
    TextField(
        value = activeConfig.position.x.toString(),
        onValueChange = { viewModel.editPosition(x = it) },
        label = { Text(text = stringResource(R.string.setup_text_fields_position_x)) },
    )
    TextField(
        value = activeConfig.position.y.toString(),
        onValueChange = { viewModel.editPosition(y = it) },
        label = { Text(text = stringResource(R.string.setup_text_fields_position_y)) },
    )
}


@Composable
private fun Padding(
    activeConfig: TextConfig,
    viewModel: MainViewModel,
) {
    Title(R.string.setup_text_fields_padding)
    TextField(
        value = activeConfig.padding.rim.toString(),
        onValueChange = { viewModel.editPadding(rim = it) },
        label = { Text(text = stringResource(R.string.setup_text_fields_padding_rim)) },
    )
    TextField(
        value = activeConfig.padding.left.toString(),
        onValueChange = { viewModel.editPadding(left = it) },
        label = { Text(text = stringResource(R.string.setup_text_fields_padding_left)) },
    )
    TextField(
        value = activeConfig.padding.right.toString(),
        onValueChange = { viewModel.editPadding(right = it) },
        label = { Text(text = stringResource(R.string.setup_text_fields_padding_right)) },
    )
    TextField(
        value = activeConfig.padding.top.toString(),
        onValueChange = { viewModel.editPadding(top = it) },
        label = { Text(text = stringResource(R.string.setup_text_fields_padding_top)) },
    )
    TextField(
        value = activeConfig.padding.bottom.toString(),
        onValueChange = { viewModel.editPadding(bottom = it) },
        label = { Text(text = stringResource(R.string.setup_text_fields_padding_bottom)) },
    )
}

@Composable
private fun Align(
    activeConfig: TextConfig,
    viewModel: MainViewModel,
) {
    Title(R.string.setup_text_fields_align)
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.padding(horizontal = 20.dp),
    ) {
        Text(
            text = stringResource(R.string.setup_text_fields_align_left),
            fontSize = 15.sp,
            modifier = Modifier.padding(horizontal = 20.dp),
        )
        RadioButton(
            selected = activeConfig.align == TextConfigAlign.Left,
            onClick = { viewModel.editingSetAlign(TextConfigAlign.Left) },
            enabled = true,
        )

        Spacer(modifier = Modifier.weight(1f))

        Text(
            text = stringResource(R.string.setup_text_fields_align_center),
            fontSize = 15.sp,
            modifier = Modifier.padding(horizontal = 20.dp),
        )
        RadioButton(
            selected = activeConfig.align == TextConfigAlign.Center,
            onClick = { viewModel.editingSetAlign(TextConfigAlign.Center) },
            enabled = true,
        )

        Text(
            text = stringResource(R.string.setup_text_fields_align_right),
            fontSize = 15.sp,
            modifier = Modifier.padding(horizontal = 20.dp),
        )
        RadioButton(
            selected = activeConfig.align == TextConfigAlign.Right,
            onClick = { viewModel.editingSetAlign(TextConfigAlign.Right) },
            enabled = true,
        )
    }
}

@Composable
private fun Lines(
    activeConfig: TextConfig,
    viewModel: MainViewModel,
) {
    Title(R.string.setup_text_fields_max_lines)

    NumberFieldWidget(
        text = activeConfig.lines.toString(),
        onIncrease = viewModel::editingMaxLinesIncrease,
        onDecrease = viewModel::editingMaxLinesDecrease,
    )

}

@Composable
private fun SizeHeight(
    activeConfig: TextConfig,
    viewModel: MainViewModel,
) {

    Title(R.string.setup_text_fields_size_height)

    NumberFieldWidget(
        text = activeConfig.size.height.toString(),
        onIncrease = viewModel::editingSizeHeightIncrease,
        onDecrease = viewModel::editingSizeHeightDecrease,
    )
}

@Composable
private fun SizeWidth(
    activeConfig: TextConfig,
    viewModel: MainViewModel,
) {

    Title(R.string.setup_text_fields_size_width)

    NumberFieldWidget(
        text = activeConfig.size.width.toString(),
        onIncrease = viewModel::editingSizeWidthIncrease,
        onDecrease = viewModel::editingSizeWidthDecrease,
    )
}







