package com.example.control.ui.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.control.ui.screen.ResultScreen
import com.example.control.ui.screen.SetupNumberScreen
import com.example.control.ui.screen.SetupTextFieldsScreen


object Destinations {
    const val setupNumber = "setupNumber"
    const val setupTextFields = "setupTextFields"
    const val result = "result"
}

@Composable
fun NavigationHost(
    modifier: Modifier = Modifier,
    navController: NavHostController = rememberNavController(),
) {

    NavHost(
        modifier = modifier,
        navController = navController,
        startDestination = Destinations.setupNumber,
    ) {
        composable(Destinations.setupNumber) {
            SetupNumberScreen(
                onNavigateNext = {
                    navController.navigate(Destinations.setupTextFields)
                },
            )
        }
        composable(Destinations.setupTextFields) {
            SetupTextFieldsScreen(
                onNavigateBack = {
                    navController.popBackStack()
                },
                onNavigateNext = {
                    navController.navigate(Destinations.result)
                }
            )
        }
        composable(Destinations.result) {
            ResultScreen(
                onNavigateBack = {
                    navController.popBackStack()
                },
            )
        }
    }

}